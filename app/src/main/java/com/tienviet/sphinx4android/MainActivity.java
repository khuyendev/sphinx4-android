package com.tienviet.sphinx4android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.cmu.sphinx.api.LiveSpeechRecognizer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
